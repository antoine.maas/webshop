import frappe

def get_authorized_modes_of_payment():
	webshop_settings = frappe.get_cached_doc("Webshop Settings")
	enabled_modes_of_payment = [
		mop.mode_of_payment for mop in webshop_settings.authorized_modes_of_payment
	]
	return frappe.get_all(
		"Mode of Payment",
		filters={"enabled": 1, "name": ("in", enabled_modes_of_payment)},
		fields=["icon", "payment_gateway", "portal_title as title", "name"],
	)